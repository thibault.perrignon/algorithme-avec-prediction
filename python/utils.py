import numpy as np


# code extracted from https://github.com/ildoge/SUP
def jobs_gen_add(num_of_Jobs:int=250,/,alpha:int=1.1,sigma:int=1,rj_max:int=0) -> tuple[list[int],list[int],list[int]]:
    """
    num_of_Jobs : number of jobs to generate
    alpha : control the shape of the pareto distribution
    sigma : controls the magnitude of error for predictions
    rj_max : maximum value of release date

    return : tuple of jobs actual processing time, jobs predicted processing time and jobs release date
    """
    
    # actual processing time
    actual_pareto = [int(round(np.random.pareto(alpha), 2) * 100) + 1 for _ in range(num_of_Jobs)]

    # error
    noise_normal = []
    for _ in range(num_of_Jobs):
        noise_normal.append(round(np.random.randn() * sigma, 2))

    # predicted processing time
    pred_normal_pareto = [
        round(actual_pareto[i] + noise_normal[i]) if round(actual_pareto[i] + noise_normal[i], 2) > 1 else 1 for i
        in range(num_of_Jobs)]

    # release date uniform random between 0 and rj_max
    release_date = [np.random.randint(rj_max+1) for _ in range(num_of_Jobs)]

    return (actual_pareto, pred_normal_pareto, release_date)

def jobs_gen_mult(num_of_Jobs:int=250,/,alpha:int=1.1,sigma:int=1,rj_max:int=0) -> tuple[list[int],list[int],list[int]]:
    """
    num_of_Jobs : number of jobs to generate
    alpha : control the shape of the pareto distribution
    sigma : controls the magnitude of error for predictions
    rj_max : maximum value of release date

    return : tuple of jobs actual processing time, jobs predicted processing time and jobs release date
    """
    
    # actual processing time
    actual_pareto = [int(round(np.random.pareto(alpha), 2) * 100) + 1 for _ in range(num_of_Jobs)]

    # error
    noise_normal = []
    for _ in range(num_of_Jobs):
        noise_normal.append(round(abs(np.random.randn()) * sigma, 2))

    # predicted processing time
    pred_normal_pareto = [
        round(actual_pareto[i] * noise_normal[i]) if round(actual_pareto[i] * noise_normal[i], 2) > 1 else 1 for i
        in range(num_of_Jobs)]

    # release date uniform random between 0 and rj_max
    release_date = [np.random.randint(rj_max+1) for _ in range(num_of_Jobs)]

    return (actual_pareto, pred_normal_pareto, release_date)


def display_result(machines:list[list[tuple[int,int,int,int]]]):
    """
    machines : each machine is display with their jobs in order of processing

    return : None
    """
    for i,machine in enumerate(machines):
        print(f"machine{i}: {list(j for j in machine)}")








