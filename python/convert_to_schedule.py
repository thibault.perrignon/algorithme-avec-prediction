from fractions import Fraction

import LPPT
import PPRR
import utils

def schedule_LPPT(jobs:tuple[list[int], list[int], list[int]],machines:list[list[tuple[int,int,int,int]]]):
    """
    jobs : list of tuple of jobs actual processing time, jobs predicted processing time and jobs release date
    machines : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,japt,jppt,rj)
    
    return : schedule use to make an svg image
    """

    string = ""

    numbermachines = len(machines)
    numberjobs = len(jobs[0])

    # largest deadline
    makespan:int = LPPT.Cmax(machines)

    # Cmax value
    objvalue:int = LPPT.Cmax(machines)
    objfct="Cmax"

    string+=f"<schedule {numberjobs=}  {numbermachines=}  {makespan=}  {objvalue=}  objfct={objfct} >\n"
    for i,machine in enumerate(machines):
        job_start = 0
        for id_job,japt,jppt,rj in machine:
            job_start = max(job_start,rj)
            string+=f"<execution job={id_job} machine={i} start={job_start} end={job_start+japt} />\n"
            job_start+=japt

    for job in enumerate(zip(*jobs)):
        string+=f"<job name={job[0]} releasetime={job[1][2]} processingtime={job[1][0]} deadline={999999999} />\n"

    string+=f"</schedule>\n"

    return string


def schedule_PPRR(jobs:tuple[list[int], list[int]],machines:list[list[tuple[int,Fraction,int,Fraction]]]):
    """
    jobs : list of tuple of jobs actual processing time, jobs predicted processing time and jobs release date
    machines : machines[m] is a list, machines[m][n] is a tuple (j_id,prop,date,process_time)
    
    return : schedule use to make an svg image
    """

    string = ""

    numbermachines = len(machines)
    numberjobs = len(jobs[0])

    # largest deadline
    makespan:int = PPRR.Cmax(machines)

    # Cmax value
    objvalue:int = PPRR.Cmax(machines)
    objfct="Cmax"

    string+=f"<schedule {numberjobs=}  {numbermachines=}  {makespan=}  {objvalue=}  objfct={objfct} >\n"
    for i,machine in enumerate(machines):
        for id_job,prop,date,process_time in machine:
            string+=f"<xexecution job={id_job} prop={prop.numerator}/{prop.denominator} machine={i} start={float(date)} end={float(date+process_time)} />\n"

    string+=f"</schedule>\n"

    return string


if __name__=="__main__":
    num_jobs = 60
    num_machines = 10

    jobs = utils.jobs_gen_add(num_jobs,alpha=15,rj_max=20)
    
    machines = LPPT.LPPT_with_rj(num_machines,*jobs) 
    print("LPPT schedule:\n")
    print(schedule_LPPT(jobs,machines))

    machines = PPRR.PPRR(num_machines,*jobs[:2])
    print("PPRR schedule:\n")
    print(schedule_PPRR(jobs[:2],machines))