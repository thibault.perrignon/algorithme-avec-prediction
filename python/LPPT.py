from utils import display_result,jobs_gen_add


def add_job_to_machines(id_job:int,job_actual_time:int,job_predicted_time:int,rj:int,machines:list[list[tuple[int,int,int,int]]]) -> int:
    """
    id_job : identifier
    job_actual_time : real processing time
    job_predicted_time : predicted processing time
    rj: release job date
    machines : list of machines with their jobs, update in place

    return : date of the first available machine
    """

    # machine with the lowest time
    selected_machine = min(machines,key=lambda m:machine_end(m))
    # add job the selected machine
    selected_machine.append((id_job,job_actual_time,job_predicted_time,rj))

    m_end_min = machine_end(machines[0])
    for m in machines[1:]:
        m_end_min = min(m_end_min,machine_end(m))

    return m_end_min


def machine_end(machine:list[tuple[int,int,int,int]]) -> int:
    end = 0
    for _,japt,_,rj in machine:
        end = max(end,rj)
        end += japt
    return end


def Cmax(machines:list[list[tuple[int,int,int,int]]]) -> int:
    cmax = 0
    for m in machines:
        m_end = machine_end(m)
        if m_end > cmax:
            cmax = m_end
    return cmax


def LPPT_with_rj(num_of_machines:int,jobs_actual_process_time:list[int],jobs_predicted_process_time:list[int],jobs_rj:list[int]) -> list[list[tuple[int,int,int,int]]]:
    """
    Longest Predicted Processing Time first

    num_of_machines : number of machines use to process jobs
    jobs_actual_process_time : real processing time
    jobs_predicted_process_time : predicted processing time
    jobs_rj : release job date

    return : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,japt,jppt,rj).
    """

    # machines[m]:list of tuple
    # machines[m][n]: (id_job,japt,jppt,rj)
    machines = [[] for _ in range(num_of_machines)]

    # jobs: id_job,japt,jppt
    jobs = enumerate(zip(jobs_actual_process_time,jobs_predicted_process_time,jobs_rj))
    sorted_jobs = sorted(jobs,key=lambda j:j[1][1],reverse=True)

    date = 0
    while len(sorted_jobs)>0:
        for i,(id_job,(japt,jppt,rj)) in enumerate(sorted_jobs):
            if rj <= date:
                #print(f"date:{date}, add jobs:{id_job,japt,jppt,rj}")
                date2 = add_job_to_machines(id_job,japt,jppt,rj,machines)
                date = max(date,date2)
                sorted_jobs.pop(i)
                break
        else:
            #print(f"date:{date}, pas de jobs dispo goto:{min(j[1][2] for j in sorted_jobs)}")
            date = min(j[1][2] for j in sorted_jobs)
    
    return machines


def LPPT(num_of_machines:int,jobs_actual_process_time:list[int],jobs_predicted_process_time:list[int]) -> list[list[tuple[int,int,int,int]]]:
    """
    Longest Predicted Processing Time first

    num_of_machines : number of machines use to process jobs
    jobs_actual_process_time : real processing time
    jobs_predicted_process_time : predicted processing time

    return : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,japt,jppt,rj).
    """
    return LPPT_with_rj(num_of_machines,jobs_actual_process_time,jobs_predicted_process_time,[0]*len(jobs_actual_process_time))


def LPT_with_rj(num_of_machines:int,jobs_process_time:list[int],jobs_rj:list[int]) -> list[list[tuple[int,int,int,int]]]:
    """
    Longest Processing Time first
    
    num_of_machines : number of machines use to process jobs
    jobs_process_time : real processing time
    jobs_rj : release job date

    return : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,jpt,jpt,rj).
    """
    return LPPT_with_rj(num_of_machines,jobs_process_time,jobs_process_time,jobs_rj)


def LPT(num_of_machines:int,jobs_process_time:list[int]) -> list[list[tuple[int,int,int,int]]]:
    """
    Longest Processing Time first
    
    num_of_machines : number of machines use to process jobs
    jobs_process_time : real processing time

    return : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,jpt,jpt,rj).
    """
    return LPPT(num_of_machines,jobs_process_time,jobs_process_time)


if __name__ == "__main__":
    num_of_machine = 5
    num_of_jobs = 50

    jobs_actual_process_time,jobs_predicted_process_time,jobs_rj = jobs_gen_add(num_of_jobs,sigma=50,rj_max=20)

    print("LPPT_with_rj result : ")
    display_result(res:=LPPT_with_rj(num_of_machine,jobs_actual_process_time,jobs_predicted_process_time,jobs_rj))
    print(f"Cmax = {Cmax(res)}")
    print("\nLPT_with_rj result : ")
    display_result(res:=LPT_with_rj(num_of_machine,jobs_actual_process_time,jobs_rj))
    print(f"Cmax = {Cmax(res)}")
    print("\nLPPT result : ")
    display_result(res:=LPPT(num_of_machine,jobs_actual_process_time,jobs_predicted_process_time))
    print(f"Cmax = {Cmax(res)}")
    print("\nLPT result : ")
    display_result(res:=LPT(num_of_machine,jobs_actual_process_time))
    print(f"Cmax = {Cmax(res)}")








