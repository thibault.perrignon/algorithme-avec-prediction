import matplotlib.pyplot as plt
from math import log,exp
from PPRR import PPRR,Cmax
from utils import jobs_gen_add,jobs_gen_mult


num_machines = 5
num_jobs = 50
num_points = 100
rj_max = 15
alpha = 1.1

X = []
Y = []
for sigma in range(0,5000,50):
    X.append(sigma)
    print(sigma)

    sample_add = [jobs_gen_add(num_jobs,alpha=alpha,sigma=sigma,rj_max=rj_max) for i in range(num_points)]
    sample_mult = [jobs_gen_mult(num_jobs,alpha=alpha,sigma=sigma,rj_max=rj_max) for i in range(num_points)]
    
    ratio_add = []
    for sample in sample_add:
        machines_PPRR = PPRR(num_machines,*sample[:-1])

        ALG = Cmax(machines_PPRR)
        L = max(sum(sample[0])/num_machines,max(sample[0]))

        ratio_add.append(ALG/L)

    ratio_mult = []
    for sample in sample_mult:
        machines_PPRR = PPRR(num_machines,*sample[:-1])

        ALG = Cmax(machines_PPRR)
        L = max(sum(sample[0])/num_machines,max(sample[0]))

        ratio_mult.append(ALG/L)

    Y.append((sum(ratio_add)/len(ratio_add),sum(ratio_mult)/len(ratio_mult)))


plt.plot(X,Y,label=["err add","err mult"])
plt.xlabel("erreur sur la durée")
plt.ylabel("rapport entre l'algorithme et l'optimal")
plt.title(f"Algo PPRR")
plt.legend(loc='upper left')
plt.show()


 