import matplotlib.pyplot as plt
from math import log,exp
from LPPT import LPPT_with_rj,LPT_with_rj,Cmax
from utils import jobs_gen_add,jobs_gen_mult


num_machines = 5
num_jobs = 250
num_points = 100
rj_max = 15
alpha = 1.1

X = []
Y = []
for sigma in range(0,5000,50):
    X.append(sigma)
    print(sigma)

    sample_add = [jobs_gen_add(num_jobs,alpha=alpha,sigma=sigma,rj_max=rj_max) for i in range(num_points)]
    sample_mult = [jobs_gen_mult(num_jobs,alpha=alpha,sigma=sigma,rj_max=rj_max) for i in range(num_points)]
    
    ratio_add = []
    for sample in sample_add:
        machines_LPPT = LPPT_with_rj(num_machines,*sample)
        machines_LPT = LPT_with_rj(num_machines,sample[0],sample[2])

        ALG = Cmax(machines_LPPT)
        OPT = Cmax(machines_LPT)

        ratio_add.append(ALG/OPT)

    ratio_mult = []
    for sample in sample_mult:
        machines_LPPT = LPPT_with_rj(num_machines,*sample)
        machines_LPT = LPT_with_rj(num_machines,sample[0],sample[2])

        ALG = Cmax(machines_LPPT)
        OPT = Cmax(machines_LPT)

        ratio_mult.append(ALG/OPT)

    Y.append((sum(ratio_add)/len(ratio_add),sum(ratio_mult)/len(ratio_mult)))



plt.plot(X,Y,label=["err add","err mult"])
plt.xlabel("erreur sur la durée")
plt.ylabel("rapport entre l'algorithme et l'optimal")
plt.title(f"Algo LPPT")
plt.legend(loc='upper left')
plt.show()