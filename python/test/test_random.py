import sys

from PPRR import PPRR
from LPPT import LPPT_with_rj
from convert_to_schedule import schedule_PPRR, schedule_LPPT
from utils import jobs_gen_add
from schedule_to_svg import svg

num_jobs = 10
num_machines = 5

jobs = jobs_gen_add(num_jobs,alpha=15,rj_max=20)

machines_PPRR = PPRR(num_machines,*jobs[:2])
sch_PPRR = schedule_PPRR(jobs[:2],machines_PPRR)
machines_LPPT = LPPT_with_rj(num_machines,*jobs)
sch_LPPT = schedule_LPPT(jobs,machines_LPPT)

schedule2svg_path = sys.argv[1]

with open("test_PPRR.svg", "w") as f:
    f.write(svg(sch_PPRR,schedule2svg_path))

with open("test_LPPT.svg", "w") as f:
    f.write(svg(sch_LPPT,schedule2svg_path))