import subprocess

class svgCreationError(Exception):
    def __init__(self, error) -> None:
        self.error = error
        super().__init__(f"An error occured while creating the svg:\n{error}")

def svg(sch,schedule2svg_path,option=("z")):
    echo = subprocess.Popen(["echo", sch], stdout=subprocess.PIPE, text=True)
    svg = subprocess.Popen(
        [schedule2svg_path, *option],
        stdin=echo.stdout,
        stdout=subprocess.PIPE,
        text=True,
    )
    output, error = svg.communicate()
    if error:
        raise svgCreationError(error)
    return output