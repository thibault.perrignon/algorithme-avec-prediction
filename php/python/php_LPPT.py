import sys

from convert_to_schedule import schedule_LPPT
from schedule_to_svg import svg

def LPPT(num_of_machines:int,jobs_actual_process_time:list[int],jobs_predicted_process_time:list[int],jobs_rj=list[int]) -> list[list[tuple[int,int,int,int]]]:
    """
    Longest Predicted Processing Time first

    num_of_machines : number of machines use to process jobs
    jobs_actual_process_time : real processing time
    jobs_predicted_process_time : predicted processing time
    jobs_rj : release job date

    return : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,japt,jppt,rj).
    """

    # machines[m]:list of tuple
    # machines[m][n]: (id_job,japt,jppt,rj)
    machines = [[] for _ in range(num_of_machines)]

    # jobs: id_job,japt,jppt
    jobs = enumerate(zip(jobs_actual_process_time,jobs_predicted_process_time,jobs_rj))
    sorted_jobs = sorted(jobs,key=lambda j:j[1][1],reverse=True)

    date = 0
    while len(sorted_jobs)>0:
        for i,(id_job,(japt,jppt,rj)) in enumerate(sorted_jobs):
            if rj <= date:
                date2 = add_job_to_machines(id_job,japt,jppt,rj,machines)
                date = max(date,date2)
                sorted_jobs.pop(i)
                break
        else:
            date = min(j[1][2] for j in sorted_jobs)
    
    return machines

def add_job_to_machines(id_job:int,job_actual_time:int,job_predicted_time:int,rj:int,machines:list[list[tuple[int,int,int,int]]]) -> int:
    """
    id_job : identifier
    job_actual_time : real processing time
    job_predicted_time : predicted processing time
    rj: release job date
    machines : list of machines with their jobs, update in place

    return : date of the first available machine
    """

    # machine with the lowest time
    selected_machine = min(machines,key=lambda m:machine_end(m))
    # add job the selected machine
    selected_machine.append((id_job,job_actual_time,job_predicted_time,rj))

    m_end_min = machine_end(machines[0])
    for m in machines[1:]:
        m_end_min = min(m_end_min,machine_end(m))

    return m_end_min

def machine_end(machine:list[tuple[int,int,int,int]]) -> int:
    end = 0
    for _,japt,_,rj in machine:
        end = max(end,rj)
        end += japt
    return end

def Cmax(machines:list[list[tuple[int,int,int,int]]]) -> int:
    cmax = 0
    for m in machines:
        m_end = machine_end(m)
        if m_end > cmax:
            cmax = m_end
    return cmax

if __name__ == "__main__":

    jobs_rj = []
    jobs_actual_process_time = []
    jobs_predicted_process_time = []
    for j in sys.argv[1].split(' '):
        if len(j)==0:
            continue
        job = j.split('/')

        jobs_rj.append(int(job[0]))
        jobs_actual_process_time.append(int(job[1]))
        jobs_predicted_process_time.append(int(job[2]))

    jobs = (jobs_actual_process_time,jobs_predicted_process_time,jobs_rj)
    num_of_machine = int(sys.argv[2])
    schedule2svg_path = sys.argv[3]

    res = LPPT(num_of_machine,*jobs)
    sch_LPPT = schedule_LPPT(jobs, res)
    print(svg(sch_LPPT,schedule2svg_path))











