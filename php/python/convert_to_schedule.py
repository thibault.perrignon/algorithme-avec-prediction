from fractions import Fraction


def schedule_LPPT(jobs:tuple[list[int], list[int], list[int]],machines:list[list[tuple[int,int,int,int]]]):
    """
    jobs : list of tuple of jobs actual processing time, jobs predicted processing time and jobs release date
    machines : machines, machines[m] is a list, machines[m][n] is a tuple (id_job,japt,jppt,rj)
    
    return : schedule use to make an svg image
    """

    string = ""

    numbermachines = len(machines)
    numberjobs = len(jobs[0])

    # largest deadline
    makespan:int = LPPT_Cmax(machines)

    # Cmax value
    objvalue:int = LPPT_Cmax(machines)
    objfct="Cmax"

    string+=f"<schedule {numberjobs=}  {numbermachines=}  {makespan=}  {objvalue=}  objfct={objfct} >\n"
    for i,machine in enumerate(machines):
        job_start = 0
        for id_job,japt,jppt,rj in machine:
            job_start = max(job_start,rj)
            string+=f"<execution job={id_job} machine={i} start={job_start} end={job_start+japt} />\n"
            job_start+=japt

    for job in enumerate(zip(*jobs)):
        string+=f"<job name={job[0]} releasetime={job[1][2]} processingtime={job[1][0]} deadline={999999999} />\n"

    string+=f"</schedule>\n"

    return string


def schedule_PPRR(jobs:tuple[list[int], list[int]],machines:list[list[tuple[int,Fraction,int,Fraction]]]):
    """
    jobs : list of tuple of jobs actual processing time, jobs predicted processing time and jobs release date
    machines : machines[m] is a list, machines[m][n] is a tuple (j_id,prop,date,process_time)
    
    return : schedule use to make an svg image
    """

    string = ""

    numbermachines = len(machines)
    numberjobs = len(jobs[0])

    # largest deadline
    makespan:int = PPRR_Cmax(machines)

    # Cmax value
    objvalue:int = PPRR_Cmax(machines)
    objfct="Cmax"

    string+=f"<schedule {numberjobs=}  {numbermachines=}  {makespan=}  {objvalue=}  objfct={objfct} >\n"
    for i,machine in enumerate(machines):
        for id_job,prop,date,process_time in machine:
            string+=f"<xexecution job={id_job} prop={prop.numerator}/{prop.denominator} machine={i} start={float(date)} end={float(date+process_time)} />\n"

    string+=f"</schedule>\n"

    return string

def machine_end(machine:list[tuple[int,int,int,int]]) -> int:
    end = 0
    for _,japt,_,rj in machine:
        end = max(end,rj)
        end += japt
    return end

def LPPT_Cmax(machines:list[list[tuple[int,int,int,int]]]) -> int:
    cmax = 0
    for m in machines:
        m_end = machine_end(m)
        if m_end > cmax:
            cmax = m_end
    return cmax

def PPRR_Cmax(machines:list[list[tuple[int,Fraction,int,Fraction]]]) -> float:
    cmax = 0
    for machine in machines:
        for _,_,date,process_time in machine:
            if (c:=date+process_time) > cmax:
                cmax = c
    return float(cmax)