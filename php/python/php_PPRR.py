import sys

from fractions import Fraction
from convert_to_schedule import schedule_PPRR
from schedule_to_svg import svg


def PPRR(num_of_machines:int,jobs_actual_process_time:list[int],jobs_predicted_process_time:list[int]) -> list[list[tuple[int,Fraction,int,Fraction]]]:
    """
    Proportional Predicted Round-Robin

    num_of_machines : number of machines use to process jobs
    jobs_actual_process_time : real processing time
    jobs_predicted_process_time : predicted processing time (don't use 0)

    return : machines, machines[m] is a list, machines[m][n] is a tuple (j_id,prop,date,process_time).
    """
    num_of_jobs = len(jobs_actual_process_time)
    # jobs[i] = (id_job,japt,jppt)
    jobs = list(zip(range(num_of_jobs),jobs_actual_process_time,jobs_predicted_process_time))
    stages:list[tuple[list[list[tuple[int,Fraction]]],Fraction]] = []
    while jobs:
        alloc, process_time = job_alloc(jobs,num_of_machines)
        # print(alloc, process_time)
        stages.append((alloc, process_time))

    return stages_to_machines(stages)

def job_alloc(jobs:list[tuple[int,int,int]],num_of_machines:int) -> tuple[list[list[tuple[int, Fraction]]], Fraction]:
    """
    jobs : jobs[i] = (id,real,predicted), is updated in place
    num_of_machines : number of machines use to process jobs

    return : allocation of jobs to each machine and process time util a machine is available
    """
    alloc:list[list[tuple[int, Fraction]]] = [[] for _ in range(num_of_machines)]
    jobs_temp = jobs.copy()
    j_end_min = sum((j[2] for j in jobs_temp))

    # mandatory allocation
    for i in range(num_of_machines):
        try:
            mand_limit = Fraction(sum(j[2] for j in jobs_temp),num_of_machines)
            mand_job = min((j for j in jobs_temp if j[2] >= mand_limit), key=lambda e:e[2])
            j_end_min = min(j_end_min,mand_job[1])
            jobs_temp.remove(mand_job)
            alloc[i].append((mand_job[0],1))
            num_of_machines-=1
        except ValueError:
            break
    #print(jobs_temp,alloc,num_of_machines,i)
    
    if len(jobs_temp) != 0:
        # other allocation
        prop = Fraction(num_of_machines,sum(j[2] for j in jobs_temp))
        prop_sum = 0
        for m_id in range(i,len(alloc)):
            while 1:
                id_job,japt,jppt = jobs_temp.pop(0)
                j_prop = prop*jppt

                j_end = Fraction(japt,j_prop)
                j_end_min = min(j_end_min,j_end)

                if prop_sum+j_prop > 1:
                    j_prop_part1 = 1 - prop_sum
                    j_prop_part2 = j_prop - j_prop_part1
                    alloc[m_id].append((id_job,j_prop_part1))
                    alloc[m_id+1].append((id_job,j_prop_part2))
                    prop_sum = j_prop_part2
                    break
                elif prop_sum+j_prop == 1:
                    alloc[m_id].append((id_job,j_prop))
                    prop_sum = 0
                    break

                alloc[m_id].append((id_job,j_prop))
                prop_sum += j_prop

    # jobs update
    for m in alloc:
        for j in m:
            j_id,prop = j
            for i,(id_job,japt,jppt) in enumerate(jobs):
                if id_job == j_id:
                    break
            else:
                print("should not happen")
            
            if (japt:=japt-prop*j_end_min) <= 0:
                jobs.pop(i)
                continue
            jobs[i] = (id_job,japt,jppt)

    return alloc, j_end_min


def stages_to_machines(stages:list[tuple[list[list[tuple[int, Fraction]]], Fraction]]) -> list[list[tuple[int,Fraction,int,Fraction]]]:
    """
    stages : stages[s] is a tuple (alloc, process_time), alloc[m][n] is a tuple (id_job,prop), m is a machine and n is a job.

    return : machines, machines[m] is a list, machines[m][n] is a tuple (j_id,prop,date,process_time).
    """
    machines = [[] for _ in range(len(stages[0][0]))]
    date = 0
    for ms,process_time in stages:
        for m,machine in zip(ms,machines):
            for j_id,prop in m:
                machine.append((j_id,prop,date,process_time))
        date += process_time
    return machines


def Cmax(machines:list[list[tuple[int,Fraction,int,Fraction]]]) -> float:
    cmax = 0
    for machine in machines:
        for _,_,date,process_time in machine:
            if (c:=date+process_time) > cmax:
                cmax = c
    return float(cmax)


if __name__ == "__main__":

    jobs_actual_process_time = []
    jobs_predicted_process_time = []
    for j in sys.argv[1].split(' '):
        if len(j)==0:
            continue
        job = j.split('/')

        jobs_actual_process_time.append(int(job[0]))
        jobs_predicted_process_time.append(int(job[1]))

    jobs = (jobs_actual_process_time,jobs_predicted_process_time)
    num_of_machine = int(sys.argv[2])
    schedule2svg_path = sys.argv[3]

    res = PPRR(num_of_machine,*jobs)
    sch_PPRR = schedule_PPRR(jobs, res)
    print(svg(sch_PPRR,schedule2svg_path,option=("z")))








