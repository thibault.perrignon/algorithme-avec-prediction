<h1>Algorithm</h1>

<?php
if (isset($_REQUEST['algo'])) {
     $algo = $_REQUEST['algo'];
}
?>

<form action=index.php>


     <?
     if (isset($_REQUEST['jobs'])) {
          $jobs = $_REQUEST['jobs'];
     } else {
          $jobs = "";
     }

     if (isset($_REQUEST['num_machines'])) {
          $num_machines = $_REQUEST['num_machines'];
     } else {
          $num_machines = 5;
     }
     ?>

     <input type="submit" name="algo" <?php if (isset($algo) && $algo == "LPPT") echo "disabled"; ?> value="LPPT">
     <input type="submit" name="algo" <?php if (isset($algo) && $algo == "PPRR") echo "disabled"; ?> value="PPRR">

</form>

<p>

     <?
     if (!$jobs && isset($algo)) {
          $n = 10;
          for ($i = 0; $i < $n; $i += 1) {
               $a = rand(0, 10);
               $b = rand(0, 10);
               $rp = rand(1, 20);
               $pp = max($rp - 1 + rand(0, 2), 1);
               $rj = min($a, $b);
               if ($algo == "LPPT") {
                    $jobs .=  "$rj/";
               }
               $jobs .=  "$rp/$pp  ";
          }
     }

     ?>

<div <? if (!isset($algo)) echo "hidden" ?>>
     <h2>Input</h2>
     <p>

     <form action=index.php disabled>
          <table>
               <tr>
                    <td align=right>
                         <? if ($algo == "LPPT") echo "release time/" ?>real processing time/predicted processing time
                    <td><input name="jobs" size=120 value="<? echo $jobs; ?>">
               <tr>
                    <td align=right>

                         Number of machines
                    <td><input name="num_machines" size=120 value="<? echo $num_machines; ?>">
          </table>
          <?
          if (isset($algo)) {
               echo "<input type='hidden' name='algo' value='$algo' />";
          }
          ?>
          <input type=submit value="Compute the schedule">
     </form>

     <p>
     <h2>Output</h2>
     <h3>
          <? echo $algo ?>
     </h3>
     <?
     echo "<img src=\"" . strtr("schedule.php?jobs=$jobs&num_machines=$num_machines&algo=$algo", " ", "+") . " \" alt='Refresh the page or recompute to fix' >";
     ?>
     <div <? if ($algo == "PPRR") echo "hidden" ?>>
          <h3>
               LPT
          </h3>
          <?
          echo "<img src=\"" . strtr("schedule.php?jobs=$jobs&num_machines=$num_machines&algo=LPT", " ", "+") . " \" alt='Refresh the page or recompute to fix' >";
          ?>
     </div>
</div>